'use strict';

const fastify = require('fastify')();
const Pool = require('pg').Pool;
  
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'pass123',
    port: 5432
});
  

  
pool.connect((err, client, release) => {
    if (err) {
        return console.error(
            'Error acquiring client', err.stack)
    }
    client.query('SELECT NOW()', (err, result) => {
        release()
        if (err) {
            return console.error(
                'Error executing query', err.stack)
        }
        console.log("Connected to Database !")
    })
})
  
fastify.get('/', (req, res, next) => {
    console.log("TEST DATA :");
    pool.query('Select * from coffee')
        .then(testData => {
            console.log(testData);
            res.send(testData.rows);
        })
})
fastify.listen(3003);
