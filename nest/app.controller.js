'use strict';
var __decorate =
  (this && this.__decorate) ||
  function(decorators, target, key, desc) {
    var c = arguments.length,
      r =
        c < 3
          ? target
          : desc === null
          ? (desc = Object.getOwnPropertyDescriptor(target, key))
          : desc,
      d;
    if (typeof Reflect === 'object' && typeof Reflect.decorate === 'function')
      r = Reflect.decorate(decorators, target, key, desc);
    else
      for (var i = decorators.length - 1; i >= 0; i--)
        if ((d = decorators[i]))
          r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var __metadata =
  (this && this.__metadata) ||
  function(k, v) {
    if (typeof Reflect === 'object' && typeof Reflect.metadata === 'function')
      return Reflect.metadata(k, v);
  };
Object.defineProperty(exports, '__esModule', { value: true });
const common_1 = require('@nestjs/common');
const Pool = require('pg').Pool;
  
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'pass123',
    port: 5432
});
    
pool.connect((err, client, release) => {
  if (err) {
      return console.error(
          'Error acquiring client', err.stack)
  }
  client.query('SELECT NOW()', (err, result) => {
      release()
      if (err) {
          return console.error(
              'Error executing query', err.stack)
      }
      console.log("Connected to Database !")
  })
})
let AppController = class AppController {

  async root() {
    console.log("qweqwewq")
    
    // const main = async () => {
    //   await pool.query('Select * from coffee')
    //   .then(testData => {
      
    //        res.send(testData.rows);
    //        return testData.rows
    //   });
    // };
    try {
      var results = await  pool.query('Select * from coffee')
      return results.rows;
    }
    catch (err) {
      throw err
    }
  }
};
__decorate(
  [
    common_1.Get(),
    __metadata('design:type', Function),
    __metadata('design:paramtypes', []),
    __metadata('design:returntype', String),
  ],
  AppController.prototype,
  'root',
  null,
);
AppController = __decorate([common_1.Controller()], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map
